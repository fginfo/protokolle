FG Treffen 15.06., 18:00 CET
===

## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de/mumble, eine WebApp die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://fachschaften.uni-goettingen.de/how-to-mumble)
### Anwesende
Jessi, Ana, Jake, Linux, Sergio, Valerius, Lorenz Mobil

### FG [Kartoffelscore](https://pad.gwdg.de/kartoffelpad)
- Linux +2
- Jessi +1


## TOPke/Disskussionthemen
### O-Phase
* FSRV: O-Phasenplaung, Ideenaustauschtreffen diese Woche kommend 
    * Auch Thema: Nordcampusralley 
* Hauptorga Treffen am Freitag (Jessi trägt die Ergebnisse weiter beim nächsten FG Treffen)


### Umfrage zu Arbeitsbedingungen von SHK
Die Umfrage existiert. Wir wurden da per Email angeschrieben, ob wir die geeignet weiterleiten könnten. Frage: Wie wollen wir die weiterleiten, an alle, gezieltes anschreiben? Das hat Vor- und Nachteile. Ente würde entsprechend des Diskussionsergebnisses was vorformulieren.
> per Studentenverteiler weiterleiten


### LSV Sitzung 
Die Info-Lehrämtler kriegen das glaube ich alle nicht mit, wenn da was stattfindet, aber potenziell waren da Leute, die vielleicht Interesse daran hätten, dahin zu gehen. Konkret: Max und Julia. Möchte die jemand mal darüber informieren?
> 🤔🤔🤔🤔🤔 verschoben auf das naechste FG.

### Informationsabend Zweitsemester
Braucht Linux noch Dinge? Da ist ein entsprechendes Treffen für den 2-Fach Bachelor angekündigt worden. Wer macht das und wann? 
* Ankündigung über den Telegramchannel
* Zustimmung für die aktuelle Version der Präsentation
* gerne eine Person, die Mittwoch dabei ist und ggf bei Fragen aushilft
* Zweifach: Julia ist angesprochen worden, die sich da aber auch noch Hilfe wünschen würde. Linux sorgt erst dafür, dass der Abend für den Monobachelor abgeschlossen ist und kümmert sich dann um den Zweifachbachelor

> Es werden Anpassungen durch Linux vorgenommen.

### Essensdinge von coronabedingt abgesagter Klausurtagung
Was tun damit? Muss mit FG data Science abgestimmt werden.

* Hauptsächlich: Dosen, Knabersachen, Nudeln, ...

> Wir lassen die Sachen liegen, bis mir mehr wissen (für eventuell durchführbare Veranstaltungen)

### Ungereimtheiten in Modulverzeichnis
Wie gehen wir mit den folgenden Problemen um?
1. Verschiedene forschungsbezogenen Praktika haben Info2 als Zugangsvoraussetzung. Info2 ist jetzt aber Wahlpflicht und Info2 als Voraussetzung wirkt unbegründet.
    * Lorenz schaut sich das mal an/ gibt das weiter
2. Deep Learning (`B.Inf.1237`) ist in UniVZ mit `B.Inf.1234` Machine Learning aufgeführt und wurde in FlexNow wohl auch so angemeldet. Das gibt aber nur 5C und keine 6C. `B.Inf.1234` taucht auch sonst nirgendwo auf, ML in diesem Semester ist `B.Inf.1236` und gibt auch 6C.
    * Brosenne melden (Linus)


### Internationale Studierende und Präsenzprüfungen
Haben wir Infos über die Einreisesituation für diese Studierenden? Darf in der Prüfungsform im Einzelfall in diesen Fällen abgewichen werden? 
Gibt es da Regelungen? 

* Nicht an Professoren antragen, sondern eher Internationle nochmal darauf hinweisen, sich an die Studienberatung zu wenden (Lorenz fragt Anne K. Schultz, eventuell macht/übernimmt sie das)

> Sonder- und Härtefallregelungen sind mit dem Dozenten abzusprechen. Das wird dann kurzfristig mit dem Studiendekan geklärt.

### sIT 2020
Statusupdate? Termin? Frame?
* Rückmeldung vom Asterix: Ersten zwei Wochen im September ??
* Lorenz will mit Bewerbung beginnen, Vorträge finden
* zwei Termine planen: einen Haupttermin (4-6.09) und einen Notfalltermin (beim Orga-Treffen)
* sIT Orgatreffen: (25.06, 18Uhr)

## Ende:   19.15Uhr

* Podcasts?
* Anwerbung zur Fachschaftsarbeit






