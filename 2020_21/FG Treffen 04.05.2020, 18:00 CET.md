FG Treffen 04.05.2020, 18:00 CET
===

## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de/mumble, eine WebApp die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://fachschaften.uni-goettingen.de/how-to-mumble)

## TOPke/Disskussionthemen

### Sprechstunden (Ente/Valerius)
- Gibt es genug Ansprechpartner?

### Dokumentation Fachgruppenarbeit (Valerius)
Lorenz und Linus haben sich mit dem GWDG Wiki auseinandergesetzt. Wir sollten es nutzen.
Es wird Vorgeschlagen dieses Feature einfach mal zu testen.
- Es kann MD.
- BETA TEST: Linux macht das.
- Er vermisst Features
    - Templates
- Lorenz will eine Struktur etablieren.
- Wiki-Treffen ist noetig.
    - Termin wird nach hinten verschoben, s.u.
- Sinn u. Zweck muss bestimmt werden
    - Oeffentlich o. Intern?
    - Warum nicht beides? :)
    - Statische Infos inkl. Blog auf Website erwuenscht (Ente/Valerius)
- Linux berichtet von Usability-Schwierigkeiten
    - Ggf. ein How-To-Wiki Eintrag dazupacken

### Bericht Lerngruppenspeeddating (Valerius)
- War erfolgreich
- Spaeter nochmal bewerben

### sIT (Valerius)
- Terminfindung bzgl. CV Situation
- Format an sich

### Videoaufzeichnungen von Veranstaltungen (Sterni)
- Lehrende wollen nicht aufgezeichnet werden
- Kooperiernder Ansatz wird vorgeschlagen
    - Rechte der Studierenden / Lehrenden sollten beruecksichtigt werden
    - Mangel an Freiwilligen
    - Linux macht Umfrage

### FG-Arbeit Verantwortungen (Valerius)
- Keksseminar / Aussenreferent (Lorenz)
    Ziele klarer definieren
    - Projekte starker verfolgen
- Drittsemesterberatung (Sergio?)
- Social Media bespielen
- Nachwuchsaquise (!!Valerius)
- SIT
- Weihnachtsvorlesung
- O-Phase
- Email-Account Betreuung (Ente?)

### KIF (48,0 in Neuland) (Ente)
- Wir reden über die Möglichkeit eines FG-gemeinsamen Einkaufs zur Ausstattung unseres jeweiligen ewigen Frühstücks
- Ak zu Lerngruppenfindung im digitalen Semester, gerade in Informatikkreisen (Valerius)
- Die KIF findet vom 20.-24. Mai statt. Wir wollen noch mehr Kiffel, weil wir nächstes Jahr die KIF ausrichten. Wir fragen nochmal Leute aus FSR-FG-Kontext

### Klausurtagung (Ente)
- Digital BBB
- August: Ein-Tages-Format
    - Fokus setzen
- Themenbasisert
    - Ophase
    - Erstiheft
    - etc.
- Essen
    - Verbrauch von der FG DS, Absprache mit Achim noetig
- FGVV?
    - Feedback von Studies einholen
    - wir brauchen es....WAACK!
    - AK waere optimal (Ente)
- Lange APP Nacht im digitalen Format?
    - soll möglichst stattfinden 
    - eher digital 
    - Valerius tut


### O-Phase WiSe2020/21 (Ente)
verschoben auf nächstes Treffen in 2 Wochen

### Hackathon (Lorenz)
> BLUBBER > FACEBOOK




















