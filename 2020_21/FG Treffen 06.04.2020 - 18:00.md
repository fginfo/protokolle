FG Treffen 06.04.2020 - 18:00
===

## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de:81/, eine WebApp die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://pad.gwdg.de/s/S1KiuTRHL#)

## TOPkte

### Klausurtagung

* Plan war Jugendherberge in Northeim
* ist jetzt aber verschoben

### sIT

* es gibt ne hauptorga (jessi, ..)
* sit wurde auch auf unbestimmte zeit verschoben
    * soll offline statt finden
* website muss überarbeitet werden
    * asterix bietet unterstützung an
* tk hat generelles interesse uns zu sponsorn
    * in den winterballunterlagen gibt es mehr

### Ausgestaltung/Anregungen zum Ablauf von FG Treffen
* muss vorerst digital statt finden
    * auf dem fachschaften-mumble
* 2 wochen rythmus oder montlich je nachdem wie viel es zu besprechen gibt

### Sommerfest IfI
* 30. juli geplant
    * unsicher ob es statt finden können wird
* https://www.notion.so/Fachgruppenarbeit-3c09f3a2345842b0a7c5f073a86ba792
* soll vegetarisch abgehalten werden
* es sollen spiele gespielt werden
* es werden helfende gesucht
* finanzierung hauptsächlich durchs institut

### master studis
* es wird master-erstis geben
* master welcome day fällt aus
* vlt gibts ne digitale alternative
* läuft so halb übern fsr

### KIF 48.0
* noch nicht klar wie sie statt finden
* infos dazu werden von der kif48.0 kommen

### KIF 49.0
* es gibt ne orga die schon viel orgat
    * für details kann die gefragt werden
* Werbung für unterstützung (insbesondere während der kif) machen
* Geos bei BuFaTa unterstuetzen
* neukiffel mit zu den nächsten beiden kifs mitnehmen

### Anschaffungen
* wasserkocher
    * muss den brandschutzanforderungen gerecht werden
    * soll nicht mehr als 40€ kosten

### Sonstiges
* Studienberatung
    * Es gibt jetzt Verena als weitere Beratungsperson
    * Brosenne hat zu viel zu tun (verwaltet jetzt eine proffessur)
* es gibt bedenken gegen zoom
    * bevorzugt sollte bbb genutzt werden
    * https://info.gwdg.de/dokuwiki/doku.php?id=de:services:mobile_working:videoconferencing_tools:start
* veranstaltungen werden am 20. beginnen

### Veranstaltungen
Organisatorische Fragen klaeren
- [ ] Info I
- [ ] Info II
- [ ] APP
- [ ] Mafia II 
- [x] StudIP Tutorial fuer Masterstudenten? (auf english)
    - [ ] Bei Luisa nachhaken (Buddy Programm)

