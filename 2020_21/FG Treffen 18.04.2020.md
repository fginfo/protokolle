# FG Treffen 18.04.2020
## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de/mumble, eine WebApp die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://fachschaften.uni-goettingen.de/how-to-mumble)

## TOPkte
### Organisation der Veranstaltungen
- Email Verweis im Infokanal
```
(English version below)
Liebe Studierende an der Informatik,

wie bereits angekündigt, beginnt am Montag, 20.4.2020, der Vorlesungsbetrieb in digitaler Form. Hierzu möchten wir gerne noch einmal einige Informationen mit Ihnen teilen.

Lehrveranstaltungen:
In UniVZ sind alle Veranstaltungen gelistet, die stattfinden. Informationen zu einzelnen Kursen und ihrer Durchführung finden Sie bei Stud.IP. Bitte melden Sie sich für dort für die jeweiligen Kurse an.

Prüfungen:
Für viele der ausgefallenen Prüfungen ist noch nicht klar, wie und wann sie nun durchgeführt werden. Wenn Prüfungen wiederaufgenommen werden, liegen mindestens 14 Tage zwischen Ankündigung und Durchführung einer Prüfung. Weitere Informationen erhalten Sie bei der entsprechenden Veranstaltung in UniVZ oder Stud.IP oder der verantwortlichen Lehrperson.

Rechnerräume Informatik:
Die Rechnerräume der Informatik bleiben bis auf weiteres geschlossen. Der entfernte Zugriff (remote login) auf den Rechnerpool des Instituts für Informatik ist für alle Studierenden möglich. Detaillierte Anleitung siehe https://doc.informatik.uni-goettingen.de/wiki/index.php/Shell.

Neue Bachelor-Lehrveranstaltungen:
Für Bachelorstudierende gibt es in diesem Semester zwei neue Vorlesungen:

- Technische Informatik (Dr. Brosenne)
- Mensch-Maschine-Interaktion (Dr. Harms)

Außerdem findet Informatik I (Prof. Manea) noch einmal statt. Weitere Informationen finden Sie bei UniVZ und Stud.IP.

Online-Tutorial für neue internationale Studierende:
Da die Einführungswoche für neue internationale Studierende dieses Semester nicht stattfinden konnte, hat Göttingen International eine Webseite erstellt, in der die verschiedenen Informationen als Online-Tutorial präsentiert werden (Studium in Göttingen, (Über-)Leben in Göttingen, FlexNow, StudIP, Wohnen in Göttingen): https://www.uni-goettingen.de/de/623575.html

Beratung und Unterstützung:
Bei Fragen oder Problemen wenden Sie sich gerne jederzeit an die Studienberatung der Informatik. Wir arbeiten weiterhin im Homeoffice, sind aber per E-Mail und zu unseren telefonischen Sprechzeiten zu erreichen:
https://www.uni-goettingen.de/de/186953.html
E-Mail: studienberatung@informatik.uni-goettingen.de
Telefonische Sprechzeiten siehe Blog: https://blog.stud.uni-goettingen.de/informatikstudiendekanat/

Allgemeine Informationen der Universität:
Bitte beachten Sie auch weiterhin die Ankündigungen der Universität zur aktuellen Situation: https://www.uni-goettingen.de/de/622779.html

Blog des Studiendekanats:
Sobald es wichtige Neuigkeiten gibt, werden wir Sie per E-Mail informieren. Weitere Informationen zu Sprechzeiten und anderen Angeboten veröffentlichen wir regelmäßig auf unserem Blog:
https://blog.stud.uni-goettingen.de/informatikstudiendekanat/


Bitte haben Sie Verständnis dafür, dass auch für die Lehrenden und Mitarbeiter*innen der Universität Göttingen die Situation neu ist und einiges in den ersten Wochen vielleicht noch nicht ganz rund läuft.

Ich wünsche Ihnen allen einen guten Start ins Semester.

Viele Grüße
Anne-Kathrin Schultz
Studienberatung Informatik
E-Mail: studienberatung@informatik.uni-goettingen.de
```

### StuKo-Bericht
- Email an die
- Jeder Lehrende organisiert seine Veranstaltungen selbst.
- Personalwandel
- HiWi Vertraege werden postialisch geregelt

### Zwischen- und Endevaluation der Vorlesungen
- Evaluation sollte stattfinden
- Aussetzung der Standartlehrevaluation
- Regelung ueber StudIP moeglich
- Substitutionsmittel erwuenscht, s. FG Mathe
- Janina kann sich vorstellen mitzuwirken

### Bericht zur Kommunikationsstrategie in den ersten beiden Vorlesungswochen (Ente)
- Treffen wurden von Organen abgehalten
- WIP, weitere Treffen u. Ankuendigungen sind geplant. Email u. social media.
- Kommunikationskanaele werden diskutiert
- rocket.chat u. Mumbleraum "FSR Mathe, Info u. DS"
    - Soll fuer Sprechstunden Raum bieten
    - Sprechstundenanwesende muessen bestimmt werden
    - 12-13 Uhr 18-19 Uhr
    - Woechentlich in den ersten Vorlesungswochen, danach 2 Wochentage im Semester
    - "Digitaler Stammtisch"

| Termin            | Personen                  |
|-------------------|---------------------------|
|Mo 20.04. 12-13 Uhr|                           |
|Mo 20.04. 18-19 Uhr| Valerius                  |
|Di 21.04. 12-13 Uhr|                           |
|Di 21.04. 18-19 Uhr|                           |
|Mi 22.04. 12-13 Uhr|                           |
|Mi 22.04. 18-19 Uhr| Linux                     |
|Do 23.04. 12-13 Uhr|                           |
|Do 23.04. 18-19 Uhr| Linux                     |
|Fr 24.04. 12-13 Uhr|                           |
|Fr 24.04. 18-19 Uhr| Valerius                  |
|Mo 27.04. 12-13 Uhr|                           |
|Mo 27.04. 18-19 Uhr| Valerius                  |
|Di 28.04. 12-13 Uhr|                           |
|Di 28.04. 18-19 Uhr|                           |
|Mi 29.04. 12-13 Uhr|                           |
|Mi 29.04. 18-19 Uhr|                           |
|Do 30.04. 12-13 Uhr|                           |
|Do 30.04. 18-19 Uhr|                           |
|Fr 01.05. 12-13 Uhr|                           |
|Fr 01.05. 18-19 Uhr|                   |

### Lerngruppen Speeddating DIGITAL
- Valerius ueberlegt sich etwas

### Verwendung von zoom
- BBB nur anwendbar fuer VA mit max. 100 Personen
- Klare Empfehlung gegen zoom seitens der GWDG
- Prof. Ecker will nur zoom nutzen
    - Achim zustaendig
    - Wenig Kooperationspotential
    - Pruefungsvorleistung erfordert eine ausfuehrliche Interaktion mit zoom
    - Er will nicht aufzeichnen.
- Datenschutzbeauftragter der Uni aeussert sich nicht
- Es wird ggf. mit der Landesdatenschutzbeauftragten geredet.
- (P) Studierende haben ggf. schlechte Ressourcen, wie Hardware, DSL, etc.

#### in Informatikveranstaltungen (Ente)
Im konkreten Fall sehr divers aufgestellte Problematik: unangebrachte Antwort (dabei keine Beantwortung gestellter Fragen, pauschalantwort, die 1:1 auch andere erhalten haben), Abwiegulung von Datenschutz und Sicherheit, Prüfungsteilnahme an zoom gekoppelt, ggf. quasi Anwesenheitspflicht.

Was kann man dagegen tun?
1. An die Studienberatung wenden (Anna)
2. mit Ecker reden (Valerius)
3. bei der Prüfungskommission beschweren, dass man bei "Machine Learning" Anwesenheitspflicht hat (Lorenz, Sergio)
4. Fakultätsratsbeschluss dagegen, dass zoom für Prüfungs(vor)leistungen verwendet wird (Lorenz)
5. Beschwerde bei Hallaschka (Lorenz)
6. Beschwerde bei der Landesdatenschutzbeauftragten (Lorenz)

#### in gremien (astarix)
### Veranstaltung: Genderaspekte der Informatik (Asterix?)

### digitales Studileben - mumble
- muss beworben werden
- `students@informatik.uni-goettingen.de` neuer Verteiler

### Verschiedenes
* Füllen des Pads mit TOPs vor Einladung
    - Intentionen auf den Punkt bringen
    - Sinn des PADs bewerben
    - "Jeder der an der Informatik studiert ist eingeladen eigene Diskussionspunkte zu ergaenzen"
* Stand der Klausurtagung
* Infrastruktur
    * Dokumentation
    * Website und Telegram
    * Pads nicht genug
    * Wiki/Blog?
        * https://info.gwdg.de/dokuwiki/doku.php?id=de:services:email_collaboration:managed_services:wiki:start
    * Welche Technologien
* Ergebnis: Alle können sich mal notion angucken 
    - www.notion.so/fginfo
    - 

#### KIF Dortmund
- Finale Entscheidungen werden am Montag getroffen



















