FG Treffen 01.06.2020, 18:00 CET
===

## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de/mumble, eine WebApp, die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://fachschaften.uni-goettingen.de/how-to-mumble)

## TOPke/Diskussionthemen

### KIF 48.0: Bericht (Bundesfachschaftenkonferenz Informatik)
- digitale Lerngruppenfindung
- FS Veranstaltungen
    - Streaming
    - Filmeabende mit lizenzfreien Filmen
        - (P) GEMA?
    - Discord um Studies zu erreichen
        - (P) Auftragsverarbeitungsvertrag
            - Uni Hannover hat dies bereits
    - Gaming Turniere
        - SuperTuxkart

### Umfrage: "Was brauchen die Studierenden gerade von der Fachschaft/-gruppe"


- Which kind of social events can you currently imagine or would you wish for? Just relate yourselves with tasks that you would usually do during your leisure time.
- What can you imagine the group could currently provide in terms of events (with social interaction)? Orientate yourself on things that you normally like to do in your free time.
- Was könnt ihr euch vorstellen, was die Fachgruppe aktuell an Veranstaltungen (mit sozialer Interkation) bereitstellen könnte? Orientiert euch an Dingen, die ihr normalerweise gerne in eurer Freizeit macht.

- Which communication tools do you most often use or how would you like to reach the FG?
- Welche Kommunikationstools nutzt ihr meist bzw. worüber wollt ihr die FG am liebsten erreichen können?
    - Email
    - Telegram
    - Discord
    - Matrix
    - Whatsapp
    - Rocketchat
    - + Sonstiges

- Do you feel that you've had enough social interaction in the past 3 weeks? How could the Fachgruppe promote this?
- Habt ihr das Gefühl, dass ihr in den letzten 3 Wochen ausreichend soziale Interaktionen hattet? Wie könnte die Fachgruppe das fördern?

### Umfrage bzgl. aktuell existierender Studienprobleme
Aus der Studienkommission:
- Es soll für alles eine Einzelfalllösung gefunden werden
- Hiwis müssten mit Equipment ausgestattet werden, dass kommt jetzt vermutlich
- Problem: Studiendekanat überträge alle Verantwortung auf die einzelnen Lehrstühle
- Information zu Prüfungen passiert 2 Wochen vorher - das kann zu knapp sein, z.B. aktuell bei Matheklausuren
- Über Prüfungstermine informieren die Lehrenden selbst und deshalb evtl. auch nicht
- Einige Prüfungen sind nicht geplant und werden erst geplant, sobald sich Studierende beschweren bzw. kritisch nachfragen
- kann sein, dass Praktika großflächig ausfallen

Was hätten wir für Fragen an die Studierenden?
- Haben Sie im Zweittermin Prüfungen verpasst, bei denen noch nicht klar ist, ob oder wann sie wiederholt werden?
- Hörst du andere Module als du ursprünglich geplant hast?
    - Ist das nachteilig für dich
- Lern- und Arbeitsverhalten
    - Kannst du aktuell arbeiten/lernen? (Arbeitsatmosphäre)
    - Ist der Workload höher oder geringer als im letzten Sommersemester / letztem Semester (für zweitsemester)?
- Technische Ausstattung ausreichend?
- finanzielle Situation / Lebenshaltungskosten
- Sonstige Probleme?

### O-Phase: Planung III
- __O-Phase: Planung IV__ nächsten FG-Termin
    - Details werden beim FSR besprochen
- Online $\rightarrow$ mehr mit der Mathe
- Offline $\rightarrow$ weniger mit der Mathe


### Drittsemester Information
Linux nimmt Kritik zum Entwurf der Folien für die Veranstaltung entgegen.

### Beteiligung Fachgruppentreffen
Es wurde geredet







