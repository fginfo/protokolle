FG Treffen 18.05.2020, 18:00 CET
===

## Kommunikation

Wir nutzen zur Kommunikation https://fachschaften.uni-goettingen.de/mumble, eine WebApp die uns erlaubt Konferenzen (ähnlich Discord/TeamSpeak/Skype) über den Browser zu halten.

[Hier findes Du eine Einrichtungsanleitung](https://fachschaften.uni-goettingen.de/how-to-mumble)

Anwesende: Anna, Daniel, Linus, Valerius, Rick, 3/4 Lorenz
## TOPke/Disskussionthemen


### O-Phase WiSe2020/21 (Ente)
* FSR hat einen AK, der vor zwei Hypothesen arbeitet:
    1. Keine Präsenzveranstaltungen
    2. Nur feste Gruppen von Größe <= 20
* Wie kann man damit arbeiten?
* alternative Zeitpläne sollen entwickelt werden, sodass man bei kommenden Änderungen der Vorgaben einen fertigen Plan bereit hat
* Tendenziell wird die O-Phase getrennter zwischen Mathe und Info -> feste Gruppen, studiengangsbezogene Informationen
* Hat der FSR das 'Übergeordnete' durchdacht, wird die Fachgruppe übernehmen
* Zum nächsten FG-Treffen sollen die Ergebnisse besprochen werden
* Data Science ist aktuell nicht im FSR und deshalb nicht an der Planung beteiligt. Allerdings wird es auch auf eine Separierung hinauslaufen

### KIF (48,0 in Neuland) (Valerius)
* Valerius liest die Teilnehmenden der KIF auf
* Jeder AK wird einen kleinen Audio- oder Videosnippet haben, in dem sein Inhalt vorgestellt wird
* Themen, die noch mit auf die KIF sollen, sollen *ASAP* in die Gruppe gepostet werden
* Die Fachgruppenvorstellung erfolgt auch über einen Audio/Videosnippet. Es gibt ein Template, das in der Email der KIF genannt wird. Die genaue Ausarbeitung der Vorstellung wird auf morgen (19.05.) verschoben.

### FG-Arbeit Verantwortungen II (Valerius)
- Drittsemsterberatung (Linux)
- Social Media (Valerius)
    - guckt mal, ob man mit Instagram und LinkedIn Werbung machen kann
    - Anmerkung zur SIT: Direktkontakt lohnt sich als Werbung am meisten
- sIT (Jessi, Lorenz)
    - ( P ) Anwesende
- O-Phase (TBA)

### Lange APP Nacht (Valerius)
* Die Übungsgruppen kristallisieren sich immer mehr heraus
* Planung soll in etwa zwei Wochen stattfinden
* Idee zur Durchführung
    * BBB mit Breakoutrooms und Musik
    * Kollektives Pizzabestellen (jeder zu sich nach Hause)
    * Idee: Lustige Statistiken aufstellen (offen, welche das sind)
    * Wettbewerb
        * Format darf der Bewertung des Projektes nicht im Wege stehen
* Anmeldung muss gemacht werden, um Verbindlichkeit zu haben
    * Meldet man sich an, bekommt man eine Mate gratis vor die Tür geliefert

### SIT
* Durchführung der SIT
    * Verschieben in den Sommer, sodass man das Wetter ausnutzen kann (Ende August, Anfang oder Ende September)
* Überschneidung der O-Phase mit SIT problematisch, weil Leute aus der Planung an beiden beteiligt sind
* Jessi schlägt vor wie die O-Phause auch die SIT auf mehrere Wege zu planen
* Valerius gibt zu Bedenken, dass die Planung zu kurzfristig ist, wenn man den Sommer mitnehmen will
    * Anna sieht das unkritisch, weil Plakate und vorlesungsfreie Zeit entfallen, sodass sich die "Arbeitsdichte" vergrößert